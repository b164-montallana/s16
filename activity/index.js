// console.log('Activity');
// let number = parseInt(prompt('Enter Value'))

// for(let i = number; i >= 0; i--) {
    
//     if(i % 10 !== 0) {
//         console.log(i);
//     }
//     if(i % 5 !== 0) {
//         console.log(i);
//         continue;
//     }
//     if(i <= 50){
//         console.log(i);
//         break;
//     }
    
// }

// let x = 'supercalifragilisticexpialidocious.';

// let vowels = "aeiou";

// for(let n = 0; n < x.length; n++){
//     console.log(x);
// }


//Solution

let number = Number(prompt('Give me a number'));
console.log("The Number you provided is:" + number + ".");

for(let count = number; count >=0; count--){

    console.log(count);

    if(count <=50) {
        console.log("The current value is at" + count + "Terminating the loop");
        break;
    }
    // if the value is divisible by 10, skip printing the number
    else if (count % 10 == 0) {
        console.log("The number is divisible by 10. Skipping the number.");
        continue;
    }
    else if (count % 5 === 0) {
        console.log('The number is divisible by 5.');
    }
}

let string = "supercalifragilisticexpialidocious.";
console.log(string);

let filteredString = " ";

for(let i = 0; i < string.length; i++){
    /*
        iteration 1:
            string[0].toLowerCase() == 'a'
            check: is 's' == 'a'
            else: filteredString += "s"
        
        iteration 2:
            i is now 1
            string[1].toLowerCase() == 'a' || 'e' || 'i' || 'o' || 'u'
            check: "u" == "u"
            continue = skip to the next iteration
        
        iteration 3:
            i is now 2
            string[1].toLowerCase() == 'a' || 'e' || 'i' || 'o' || 'u'
            check: "p" !== 'a' || 'e' || 'i' || 'o' || 'u'
            else: filteredString += "p"
            filteredString = "sp"

    */

    if (
        string[i].toLowerCase() == "a" ||
        string[i].toLowerCase() == "e" ||
        string[i].toLowerCase() == "i" ||
        string[i].toLowerCase() == "o" ||
        string[i].toLowerCase() == "u"
    ) {
        continue;

    } else {
        filteredString += string[i];
    }

}
console.log(filteredString);