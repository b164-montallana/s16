console.log('Hello world');



// Loops


// While Loop

/*
Syntax:
    while(expression/condition) {
        statement
    }

*/



// Create a function that could displayMsgToSelf()
    // -display a message to your past self in your console 10 times
    // invoke the function 10 times

function displayMsgToSelf() {
    console.log('Dont text her back.');
}

displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();


// While Loop

let count = 10;

while (count !== 0) {
    console.log("Dont text her back.");
    count--; //This is a decrementation
}
// if no decrementation it will run infinite loop.


let count1 = 5;

while(count1 !== 0) {
    console.log(count1);
    count1--;
}


// Mini activity
// The ff while loop should display the numbers from 1-5 in order

let count2 = 1;

while(count2 <= 5) {
     console.log(count2);
     count2++;
}

// Do while loop


// A do while loop is like a while loop. But do-while guarantee that the code will be executed at least once.

/*
Syntax:
    do {
        statement
    } while (expression/condition)

*/
console.log('DO-WHile loop');
let doWhileCounter =1;

do {
    console.log(doWhileCounter)
    doWhileCounter++
} while (doWhileCounter <= 20);


// let number = Number(prompt('Give me a number'));

// do {
//     console.log('Do while', + number);
//     // Increase the value of number by 1 after every iteration to stop the loop when it reaches 10
//     // number = number +1;
//     number += 1;
//  } while(number < 10)


//  For loop
/*
It consists of three parts:

1.Initialization - value that will track the progression of the loop
2.Expression/Condition that will be evaluated which will determine whether the loop will run one more time.
3.final Expression indicates how to advance the loop.


SYntax:
    for(initialization; expression/condition; finalExpression) {
        statement
    }

*/

console.log('For Loop');
// loop from 0-20

for (let count = 0; count <=20; count++) {
    console.log(count);
}

// For loops accessing array items

let fruits = ['Apple', 'Durian', 'Kiwi', 'Pineapple', 'Mango', 'Orange']

console.log(fruits[2]);
console.log(fruits.length);
console.log(fruits[fruits.length-1]); //To check the last item in an array

// show the items in an array using loops

for(let index = 0; index < fruits.length; index++) {
    console.log(fruits[index]);
}

// Mini Activity

let country = ['Japan', 'South Korea', 'Philippines', 'Canada', 'Australia', 'USA']

for(let index = 0; index < country.length-1; index++) {
    console.log(country[index]);
}


// For Loops Accessing elements of a string

let myString = "alex";
console.log(myString.length);
console.log(myString[0]);

for (let x = 0; x < myString.length; x++) {
    console.log(myString[x]);
}

let myName = 'Jane';

/*
Create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is a vowel

*/

for(let i = 0; i < myName.length; i++) {
    // if the character of your name is a vowel letter, display number '3'
    //console.log(myName[i]);
    if (
        myName[i].toLowerCase() == "a" || 
        myName[i].toLowerCase() == "e" || 
        myName[i].toLowerCase() == "i" || 
        myName[i].toLowerCase() == "o" || 
        myName[i].toLowerCase() == "u" 
    ) {
        console.log(3);
    } else {
        console.log(myName[i]);
    }
}


// Continue and Break statements.
// Continue statements allows the code to go to the next iteration of the loop withour finishing the execution of all the statements in a code block.

// break statement is used to terminate the current loop once a match has been found.


console.log('Continue and Break statemnt');

for(let count = 0; count <= 20; count++) {
    // if remainder is equal to 0, we will use the continue statement
    if(count % 2 === 0) {
        continue; // tells the code to continue to the next iteration
    }
    console.log('Continue and break', + count);

    if(count > 10) {
        break;
    }
}

// create a loop that will iterate based on the length of the string

let name = "alexandro";

for(let i = 0; i < name.length; i++) {
    console.log(name[i]);
    //if the vowel is equal to a, continue to the next iteration of the loop
    if(name[i].toLowerCase() === 'a') {
        console.log("Continue to the next");
        continue;
    }
    if(name[i] == 'd') {
        break;
    }
}



console.log('For Loop Counting 1-10')
// For Loop

for (let i = 1; i <=10; i++) {
    console.log(i);
}


console.log('While Loop counting 1-10');
// While Loop
let n = 1;

while (n <=10) {
    console.log(n);
    n++;
}

console.log('Do while loop counting Doritos');
// Do while loop

let doritos = 100;

do {
    console.log('With' + doritos + 'Doritos left I can eat');
    doritos -= 20;
} while (doritos > 10);

console.log('My Doritos are gone now.');



console.log('For in loop used in array');
// For in Loop

let colors = ['red', 'pink', 'yellow'];

for (let x in colors){
    console.log(colors[x]);
}

console.log('For in Loop use in object');

let person = {
    name: 'Tom',
    weight: 60,
    age: 40
};

for (let data in person) {
    console.log('This persons ' + data + " " + person[data]);
}